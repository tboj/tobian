## TBOJ Openbox Debian compilation

#### TObian strives to be a minimal Debian installation with a fully working Openbox environment for personal use.

To configure your minimal Debian system according to the TBOJ preferences, please do the following.  
This will install and configure `Openbox` with a `Tint2` panel and some basic applications and a great (if you're TBOJ) set of keybinds.

Terminal emulator (TBD): `Kitty`  
File manager (TBD): `Thunar`, `PCmanFM` or `SpaceFM`  
Web browser: `Vivaldi`  
Text editor (TBD): `Mousepad`  

1. Install Debian minimal
    - 1a) Download the ~~"standard+nonfree"~~ debian-[version]-amd64-netinst.iso image from the following link:
        - https://cdimage.debian.org/images/release/current/amd64/iso-cd/
        - ~~https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/current-live/amd64/bt-hybrid/~~
    - 1b) Flash the ISO to your favorite USB thumbdrive. I recommend `Etcher`.
        - https://www.balena.io/etcher/ or
        - `sudo dd if=/path/to/image.iso of=/dev/sdx bs=4M status=progress conv=fdatasync`
    - 1c) Boot your computer from the thumbdrive.
    - 1d) Following the onscreen instructions but don't install any desktop environment.
2. Login to your user. This will be in a console.
3. Download the TObian packages.
    - Install git with `sudo apt install git`.
    - Run `git clone https://gitlab.com/tboj/tobian.git`.
4. Run the install script.
    - `cd tobian/`
    - `sh install.sh`
5. Login to your freshly installed operating system and create something that makes the world a better place. 
