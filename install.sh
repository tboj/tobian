#!/bin/bash

echo “TObian: TBOJ Openbox Debian OS config”
sleep 1s
echo “Customized for laptops”
sleep 1s
echo “Press Ctrl+c at any time to halt the script. Not recommended though.”
sleep 2s

echo -e "\a\n\n\nUpdating repositories and installing packages. Please provide your superuser password when asked."

# Edit Sources.list to include main, contrib and non-free as well as backports.
# https://wiki.debian.org/SourcesList
# https://wiki.debian.org/DebianRepository/Unofficial?action=show&redirect=UnofficialRepositories

## New. Code ran in 2021, but not 2024.
sudo dpkg --configure -a
## End New.

sudo sh -c 'echo "deb http://deb.debian.org/debian/ stable main contrib non-free" >> /etc/apt/sources.list'
sudo sh -c 'echo "deb http://deb.debian.org/debian-security/ stable/updates main contrib non-free" >> /etc/apt/sources.list'
sudo sh -c 'echo "deb http://deb.debian.org/debian/ stable-proposed-updates main contrib non-free" >> /etc/apt/sources.list'
sudo sh -c 'echo "deb http://deb.debian.org/debian/ stable-updates main contrib non-free" >> /etc/apt/sources.list'
sudo sh -c 'echo "deb http://deb.debian.org/debian/ stable-backports main contrib non-free" >> /etc/apt/sources.list'
# sudo echo "deb http://deb.debian.org/debian/ stable main contrib non-free" > /etc/apt/sources.list
# sudo echo "deb http://deb.debian.org/debian-security/ stable/updates main contrib non-free" >> /etc/apt/sources.list
# sudo echo "deb http://deb.debian.org/debian/ stable-proposed-updates main contrib non-free" >> /etc/apt/sources.list
# sudo echo "deb http://deb.debian.org/debian/ stable-updates main contrib non-free" >> /etc/apt/sources.list
# sudo echo "deb http://deb.debian.org/debian/ stable-backports main contrib non-free" >> /etc/apt/sources.list
# https://stackoverflow.com/questions/850730/how-can-i-append-text-to-etc-apt-sources-list-from-the-command-line

# Update/Upgrade
sudo apt update && sudo apt full-upgrade -y

# Install packages
echo -e "\n\n\nInstalling graphics stack, display manager and core utils."
sudo apt install -y xorg xdotool xdm xscreensaver compton kitty network-manager-gnome network-manager-openvpn-gnome wget
# sudo apt install --install-recommends -y xorg xdotool xdm xscreensaver compton kitty network-manager-gnome network-manager-openvpn-gnome

echo -e "\n\n\nInstalling window manager: Openbox and pseudo-DE."
sudo apt install -y openbox obconf obmenu lxhotkey-plugin-openbox tint2 feh conky scrot rofi parcellite lxappearance nitrogen arandr
# sudo apt install --install-recommends -y openbox obconf openbox-menu tint2 feh scrot rofi parcellite lxappearance nitrogen arandr
# lxhotkey-plugin-openbox "enhances" Openbox

echo -e "\n\n\nInstalling audio utils."
# Check these packages! Do web browsers work without pulseaudio?
sudo apt install -y pnmixer alsa-utils
# https://packages.debian.org/buster/pipewire
# sudo apt install --install-recommends -y pipewire
# sudo apt install --install-recommends -y gstreamer0.10-plugins-base gstreamer0.10-plugins-good gstreamer0.10-plugins-bad pulseaudio gstreamer0.10-pulseaudio pavucontrol

echo -e "\n\n\nInstalling file management."
echo -e "\nCLI file manager: nnn; GUI file manager: Nemo"
sudo apt install -y nemo nnn
# sudo apt install --install-recommends --install-suggests -y nemo nnn
# Alternatives: thunar, pcmanfm, spacefm

echo -e "\n\n\nInstalling desktop (and laptop) utils."
sudo apt install -y gdebi tlp mousepad neofetch links2 btop
# sudo apt install --install-recommends -y gdebi tlp mousepad neofetch links2 htop

echo -e "\n\n\nInstalling web browser: Vivaldi."
wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | gpg --dearmor | sudo dd of=/usr/share/keyrings/vivaldi-browser.gpg
echo "deb [signed-by=/usr/share/keyrings/vivaldi-browser.gpg arch=$(dpkg --print-architecture)] https://repo.vivaldi.com/archive/deb/ stable main" | sudo dd of=/etc/apt/sources.list.d/vivaldi-archive.list
# wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | sudo apt-key add -
# sudo add-apt-repository 'deb https://repo.vivaldi.com/archive/deb/ stable main'
sudo apt update && sudo apt install vivaldi-stable

#############################################################################################
# Packages to be considered
# BLUETOOTH: bluez bluez-alsa bluez-gstreamer bluez-audio bluez-utils
# MEDIA: ubuntu-restricted-extras vlc audacity lame flac
# PRINTING: cups-bsd cups-driver-gutenprint cups-pdf cups-pk-helper printer-driver-hpcups printer-driver-postscript-hp python-cups python-cupshelpers system-config-printer-gnome
# MISC: menu qiv (ayatana-)indicator-application
# POWER/UTILS: xfce4-power-manager xfce4-sensors-plugin gnome-system-monitor
#############################################################################################

# Clean the system
echo -e "\n\n\nCleaning the system from old and unwanted packages."
sudo apt autoclean -y
sudo apt clean -y
sudo apt autoremove -y

#############################################################################################
#
#
## 240607: Uncommenting this. Testing to automate.
cd
cd tobian/config
cp -R -v * $HOME/.config
cd

cd $HOME
touch .xsession
echo “exec openbox-session” > .xsession
sudo chmod u+x .xsession
cd
# https://www.x.org/releases/X11R7.6-RC1/doc/man/man1/xdm.1.xhtml#heading7
sudo sh -c 'mkdir -p /usr/lib/X11/xdm'
cd /usr/lib/X11/xdm
sudo touch xdm-config
sudo sh -c 'echo "DisplayManager.servers: /usr/lib/X11/xdm/Xservers" > xdm-config'
sudo sh -c 'echo "DisplayManager.errorLogFile: /var/log/xdm.log" >> xdm-config'
sudo sh -c 'echo "DisplayManager*resources: /usr/lib/X11/xdm/Xresources" >> xdm-config'
sudo sh -c 'echo "DisplayManager*startup: /usr/lib/X11/xdm/Xstartup" >> xdm-config'
sudo sh -c 'echo "DisplayManager*session: /usr/lib/X11/xdm/Xsession" >> xdm-config'
sudo sh -c 'echo "DisplayManager.pidFile: /var/run/xdm-pid" >> xdm-config'
sudo sh -c 'echo "DisplayManager._0.authorize: true" >> xdm-config'
sudo sh -c 'echo "DisplayManager*authorize: false" >> xdm-config'
cd
cd
cd
cd

echo -e "\n."
sleep 1s
echo -e "\n."
sleep 1s
echo -e "\n."
sleep 1s
echo -e "\n."
sleep 1s
echo -e "\n."
sleep 1s
echo -e "\n."
echo -e "\nDone. Enter your superuser password one more time to reboot."

sudo shutdown -r 0

EOF

#############################################################################################
# https://patranella.wordpress.com/2014/10/02/taking-control-of-your-computer-ubuntu-minimal-install-with-openbox/
# Note: If necessary, you can make the script executable by running the command: chmod +x [filename].sh
# Note: Run the script with the command: sh filename.sh
# Do NOT run the script as sudo. The various commands will use sudo when needed.
#
# #Create Files
# touch ~/.config/openbox/autostart
#
# echo “Filling in autostart information.”
# #Fill in information to autostart file
# #Note “>” means overwrite everything, “>>” means append existing
# echo “# These programs will run after Openbox has started” >> ~/.config/openbox/autostart
# echo “numlockx &” >> ~/.config/openbox/autostart
# echo “xfce4-panel &” >> ~/.config/openbox/autostart
# echo “xfce4-power-manager &” >> ~/.config/openbox/autostart
# echo “nitrogen –restore &” >> ~/.config/openbox/autostart
# echo “(sleep 3s && volti) &” >> ~/.config/openbox/autostart
# echo “(sleep 5s && conky) &” >> ~/.config/openbox/autostart
# echo “xfdesktop &” >> ~/.config/openbox/autostart
#
# https://prahladyeri.com/blog/2016/02/minimal-debian-desktop-setup.html
# apt-get install network-manager
# apt-get install xorg openbox xdm
# apt-get install xbacklight pcmanfm lxappearance lxpanel gmrun gnome-terminal